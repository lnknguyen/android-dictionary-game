package fi.metropolia.translatorskeleton.model;

/**
 * Created by petrive on 23.3.16.
 */
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import fi.metropolia.translatorskeleton.helper.Mapper;
import fi.metropolia.translatorskeleton.utility.SharedPreferencesManager;
import fi.metropolia.translatorskeleton.helper.Constant;
import fi.metropolia.translatorskeleton.helper.VolleyService;


/**
 *
 * @author petrive
 *
 * Dictionary controller has the main logic of the application
 * as well as the UI.
 */
public class DictionaryController implements TimeOutObserver {


    private static int TIMEOUT = 5000;//five secs timeout
    private User user;
    private Dictionary dictEngFin;
    private Dictionary dictFinEng;

    private boolean timeIsOut;
    private Context context;

    public DictionaryController(Context context_) {
        context = context_;
        dictEngFin = new Dictionary("eng","fin");
        dictFinEng = new Dictionary("fin","eng");
        refresh();
    }

    private void refresh() {
        user = MyModelRoot.getInstance().getUserData().getUser();
        if ( MyModelRoot.getInstance().getUserData().getDictionary("engfin") != null)
            dictEngFin = MyModelRoot.getInstance().getUserData().getDictionary("engfin");
        if ( MyModelRoot.getInstance().getUserData().getDictionary("fineng") != null)
            dictFinEng = MyModelRoot.getInstance().getUserData().getDictionary("fineng");
    }

    public void start() {
        TrackRecord record = new TrackRecord();
        updateDictionary();
        System.out.println(record.toString());
    }

    private void doQuiz(Quiz q) {

    }

    public void addWords(String finWord, String enWord) throws JSONException {

            JSONObject obj = new JSONObject();
            obj.put("fin", finWord);
            obj.put("en", enWord);


            if (finWord.length()<2 || enWord.length()<2){
                AlertDialog alert = alertDialogBuilder("Fail ","Invalid input").create();
                alert.show();
                return;
            }

            RequestQueue queue = VolleyService.getInstance(context).getRequestQueue();

            JsonArrayRequest newRequest = new JsonArrayRequest(Request.Method.POST, Constant.BASE_API_URL + "/word", obj, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    Log.d("upload","success");
                    System.out.println(response.toString());
                    AlertDialog alert = alertDialogBuilder("Successful","Database updated").create();
                    alert.show();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("upload error",error.toString());
                    error.printStackTrace();

                    AlertDialog alert = alertDialogBuilder("Fail","Word exists or error uploading").create();
                    alert.show();
                }
            });

            queue.add(newRequest);


    }

    private void deleteWords() {

    }

    public void saveDictionary(Dictionary d) {
        String key = d.getLang1().equals("fin") ? Constant.FIN_TO_EN_PREF_KEY : Constant.EN_TO_FIN_PREF_KEY;
        SharedPreferencesManager.getInstance(context).saveToPref(d,Constant.DICTIONARY_PREF,key);
    }

    public Dictionary reloadDictionary(String key) {
        return SharedPreferencesManager.getInstance(context).loadFromPref(Dictionary.class,Constant.DICTIONARY_PREF,key);

    }

    private void updateDictionary() {
        queryAllWords();
    }

    @Override
    public void timeout() {
        //this method is called by the timeout thread
        //closing the scanner will cause illegalstateexception
        //lets write a new line to System.
        timeIsOut = true;
    }

    public void doStatus() {
        System.out.println(user.getTrackRecord());
    }

    public void saveUser() {
        if (null == user)
            user = new User("default");
        SharedPreferencesManager
                .getInstance(context)
                .saveToPref(user, Constant.USER_PREF, Constant.USER_PREF);
//        User u = SharedPreferencesManager.getInstance(context).loadFromPref(User.class,Constant.USER_PREF,Constant.USER_PREF);

    }

    public User reloadUser() {
        JsonObject obj = SharedPreferencesManager
                .getInstance(context)
                .loadFromPref(JsonObject.class,Constant.USER_PREF,Constant.USER_PREF);
        JsonObject objGet = obj.get("trackRecord").getAsJsonObject().get("totals").getAsJsonObject();
        System.out.println("track record ne: " + objGet.toString());
        HashMap<Dictionary,HashMap<String,Integer>> hashMap = new HashMap<>();
        Set<Map.Entry<String,JsonElement>> entries = objGet.entrySet();
        for (Map.Entry<String, JsonElement> entry: entries) {
            System.out.println(entry.getKey());
        }
        System.out.println("Reloaded: " + obj.toString());
        return null ;
    }

    private void save(String filename) {

    }

    private UserData reload(String filename) {
        return null;
    }

    private void queryAllWords() {
        Gson gson = new Gson();
        RequestQueue queue = VolleyService.getInstance(context).getRequestQueue();
        JsonArrayRequest jsonObject = new JsonArrayRequest
                (Constant.BASE_API_URL + "/word", new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            for (int i=0; i<response.length(); i++){
                                String finWord = response.getJSONObject(i).getString("fin");
                                String enWord = response.getJSONObject(i).getString("en");
                                String imageId = response.getJSONObject(i).getString("img_url");
                                dictFinEng
                                        .addPair(finWord, enWord);
                                dictEngFin
                                        .addPair(enWord, finWord);

                                    Mapper.getInstance().getMap().put(finWord, imageId);

                            }
                            SharedPreferencesManager
                                    .getInstance(context)
                                    .saveToPref(dictFinEng, Constant.DICTIONARY_PREF, Constant.FIN_TO_EN_PREF_KEY);
                            SharedPreferencesManager
                                    .getInstance(context)
                                    .saveToPref(dictEngFin, Constant.DICTIONARY_PREF, Constant.EN_TO_FIN_PREF_KEY);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });
        queue.add(jsonObject);
    }

    private AlertDialog.Builder alertDialogBuilder(String title,String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        return builder;
    }

}