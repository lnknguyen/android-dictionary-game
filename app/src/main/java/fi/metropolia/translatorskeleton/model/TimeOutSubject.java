package fi.metropolia.translatorskeleton.model;

/**
 * Created by petrive on 23.3.16.
 */
/**
 *
 * @author petrive
 */
public interface TimeOutSubject {
    void registerTimeOutObserver(TimeOutObserver o);
    void removeTimeOutObserver(TimeOutObserver o);
    void notifyTimeOutObservers();
}
