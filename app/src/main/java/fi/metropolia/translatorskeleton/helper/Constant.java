package fi.metropolia.translatorskeleton.helper;

/**
 * Created by nguyenluong on 4/17/16.
 */
public class Constant {
    public static final String DEFAULT = "default";
    public static final String DICTIONARY_PREF = "dictionary_pref";
    public static final String TRACK_RECORD_PREF = "trackrecord_pref";
    public static final String USER_PREF = "user_pref";
    public static final String FIN_TO_EN_PREF_KEY = "fin_to_en_pref_key";
    public static final String EN_TO_FIN_PREF_KEY = "en_to_fin_pref_key";
    public static final String TIMEOUT_KEY = "time_out_key";
    public static final String QUIZZLENGTH_KEY = "quizz_length_key";
    public static final String RADIO_GROUP_KEY = "radio_group_key";
    public static final Integer QUIZZLENGTH_THRESHOLD = 6; // min value for quizzlength
    public static final Integer TIMEOUT_THRESHOLD = 10; // min value for timeout
    public static final String HIGH_SCORE_KEY = "high_score_key"; // store crrent high score
    public static final String BASE_API_URL = "https://mysterious-inlet-23105.herokuapp.com/api/v1";
    //public static final String BASE_API_URL = "http://127.0.0.1:3000/api/v1";
}
