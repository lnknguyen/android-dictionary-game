package fi.metropolia.translatorskeleton.helper;

import android.content.Context;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by nguyenluong on 4/20/16.
 */
public class WebService {

    public void queryAllWords(Context context){

        RequestQueue queue = VolleyService.getInstance(context).getRequestQueue();
        JsonArrayRequest jsonObject = new JsonArrayRequest
                (Constant.BASE_API_URL + "/word", new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("Res:", response.toString());
                        try {
                            JSONObject obj = response.getJSONObject(0);
                            String fin = obj.getString("fin");

                            Log.d("fin",fin);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });
        queue.add(jsonObject);

    }
}
