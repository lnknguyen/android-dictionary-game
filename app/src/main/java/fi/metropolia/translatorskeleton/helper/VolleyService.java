package fi.metropolia.translatorskeleton.helper;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by nguyenluong on 4/20/16.
 */
public class VolleyService {
    private static VolleyService instance;
    private RequestQueue requestQueue;
    private static Context context;

    private VolleyService(Context context){
        requestQueue = Volley.newRequestQueue(context);

    }

    public static VolleyService getInstance(Context context){
        if (instance == null)
            instance = new VolleyService(context);
        return instance;
    }

    public RequestQueue getRequestQueue(){
        return requestQueue;
    }
}
