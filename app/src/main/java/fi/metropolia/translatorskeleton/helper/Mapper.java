package fi.metropolia.translatorskeleton.helper;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by nguyenluong on 4/25/16.
 */

/*
* Create a key-value mapping
* E.g: map a dictionary to its image id
* */
public class Mapper {
    private static Map<String,String> wordToId;
    private static Mapper instance;

    private Mapper(){
        wordToId = new HashMap<String,String>();
    }

    public static Mapper getInstance(){
        if (instance == null)
            instance = new Mapper();
        return instance;
    }

    public static Map<String,String> getMap(){
        return wordToId;
    }


}
