package fi.metropolia.translatorskeleton.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import fi.metropolia.translatorskeleton.helper.Constant;
import fi.metropolia.translatorskeleton.model.Dictionary;
import fi.metropolia.translatorskeleton.model.TrackRecord;
import fi.metropolia.translatorskeleton.model.User;

/**
 * Created by LnkNguyen on 4/21/2016.
 */


public class SharedPreferencesManager {
    private static SharedPreferencesManager instance;
    private static Context context;

    /*
    *Private constructor
    * @param context context from activity
    * @return nothing
    * */
    private SharedPreferencesManager(Context context){
        this.context = context;
        SharedPreferences sharedPref = context.getSharedPreferences(Constant.DICTIONARY_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
    }

    /*
    * Entry point to instance
    * @param context context from activity
    * @return entry point to singleton instance of this class
    * */
    public static SharedPreferencesManager getInstance(Context context){
        if (instance == null)
            instance = new SharedPreferencesManager(context);
        return instance;
    }

    /*
    * Save data to a preference
    * @param obj a generic object
    * @param prefName name of preference to save to
    * @param key key to load from preference
    * @return nothing
    * */
    public  <T> void saveToPref(T obj, String prefName,String key){
        SharedPreferences sharedPref = context.getSharedPreferences(prefName,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        String objString;
        Gson gson = new Gson();
        if (obj.getClass() == User.class){
            //objString = gson.toJsonTree(obj).getAsString();
            JsonElement ele = gson.toJsonTree(obj);
            JsonObject jsonObj = ele.getAsJsonObject();
            objString = jsonObj.toString();
        } else
            objString = gson.toJson(obj);
        editor.putString(key, objString);
        editor.commit();
    }

    /*
    * Load generic data from a preference
    * @param prefName name of preference to load from, is Constant.DEFAULT if no name is specified
    * @param key key to load from preference
    * @return a generic value from given key, null if key is not found in preference
    * */

    public <T> T loadFromPref(final Class<T> aClass,String prefName, String key){
        SharedPreferences sharedPref = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        T temp = null;
        if (sharedPref.contains(key)){
            String jsonString = sharedPref.getString(key,null);
            Gson gson = new Gson();
            if (aClass==JsonObject.class){
                JsonParser parser = new JsonParser();
                JsonObject obj = parser.parse(jsonString).getAsJsonObject();
                temp = (T)obj;
            }
            else
                temp = gson.fromJson(jsonString,aClass);
        }
        else
            return null;
        return  temp;
    }

    /*
    * Validate a shared preferences
    * @param prefName name of preference to load from, is Constant.DEFAULT if type is primitive
    * @param key key to load from preference, is the same to prefname is type is of Serializable
    * @return true if key exists , false otherwise
    * */
    public static boolean valid(String prefName, String key){
        SharedPreferences sharedPreferences = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        return sharedPreferences.contains(key);
    }
    /*
    * Temporary service
    * Reset shared preferences
    * */
    public static void clearPref(String prefName, String key){
        SharedPreferences sharedPref = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.commit();
    }


}

