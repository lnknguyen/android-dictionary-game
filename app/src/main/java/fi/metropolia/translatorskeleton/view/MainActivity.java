package fi.metropolia.translatorskeleton.view;


import android.content.SharedPreferences;
import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;


import android.support.v7.widget.Toolbar;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import org.apache.axis.constants.Use;

import fi.metropolia.translatorskeleton.R;
import fi.metropolia.translatorskeleton.fragments.FirstFragment;
import fi.metropolia.translatorskeleton.fragments.FourthFragment;
import fi.metropolia.translatorskeleton.fragments.SecondFragment;
import fi.metropolia.translatorskeleton.fragments.ThirdFragment;
import fi.metropolia.translatorskeleton.helper.Constant;
import fi.metropolia.translatorskeleton.model.Quiz;
import fi.metropolia.translatorskeleton.model.RandomQuiz;
import fi.metropolia.translatorskeleton.model.TrackRecord;
import fi.metropolia.translatorskeleton.model.UserData;
import fi.metropolia.translatorskeleton.model.MyModelRoot;
import fi.metropolia.translatorskeleton.model.User;
import fi.metropolia.translatorskeleton.model.Dictionary;
import fi.metropolia.translatorskeleton.model.DictionaryController;
import fi.metropolia.translatorskeleton.utility.FlickrManager;
import fi.metropolia.translatorskeleton.utility.SharedPreferencesManager;

public class MainActivity extends AppCompatActivity implements
        FirstFragment.OnFragmentInteractionListener, SecondFragment.OnFragmentInteractionListener ,
        ThirdFragment.OnFragmentInteractionListener, FourthFragment.OnFragmentInteractionListener{
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private DictionaryController dictController;
    private Dictionary finToEng;
    private Dictionary engToFin;
    private Quiz defaultQuiz;
    private TrackRecord trackRecord;
    private int quizzLength;
    private Gson gson;
    private User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dictController = new DictionaryController(this.getApplicationContext());
        dictController.start();
        gson = new GsonBuilder().setPrettyPrinting().create();
        user = new User("default");
        //testLoadUser();
        SharedPreferencesManager.getInstance(this).saveToPref(user,Constant.USER_PREF,Constant.USER_PREF);
       initFromPref();

        //setupt toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = (TabLayout)findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

    }


    private void testLoadUser(){
        finToEng = dictController.reloadDictionary(Constant.FIN_TO_EN_PREF_KEY);
        System.out.println("dict: " + finToEng.toString());
        RandomQuiz quiz = new RandomQuiz(5,finToEng);
        user.addQuiz(quiz);

        //UserData us = gson.fromJson(str,UserData.class);


        //String usr = gson.toJson(user);
        JsonElement usr = gson.toJsonTree(user);
        String storeStr = usr.toString();
        JsonParser parser = new JsonParser();
        JsonObject obj = parser.parse(storeStr).getAsJsonObject();
       // JsonElement ele = parser.parse(storeStr);
        System.out.println("object ne: " + obj.toString());
        JsonObject objGet = obj.get("trackRecord").getAsJsonObject().get("totals").getAsJsonObject();
        System.out.println("track record ne: " + objGet.toString());
        Set<Map.Entry<String,JsonElement>> entries = objGet.entrySet();
        for (Map.Entry<String, JsonElement> entry: entries) {
            System.out.println("key:"+entry.getKey());
            Map<String,Integer> map = gson.fromJson(entry.getValue(), new TypeToken<Map<String, Integer>>(){}.getType());
            Dictionary dict = gson.fromJson(entry.getKey(),Dictionary.class);
            System.out.println("record : " + map.toString());
        }

        dictController.saveUser();
        dictController.reloadUser();
       // User user = gson.fromJson(usr,User.class);

    }
    @Override
    protected void onResume(){
        super.onResume();
        initFromPref();

        //manager.testFlickr();
    }

    private void setupViewPager(ViewPager viewPager){
        //Create adapter for view pager
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        /*
        * Add 4 fragments to the adapter
        * Note: According to Google's document: never use non-default constructor for fragment
        * Use bundle to store data instead
        * */
        FirstFragment firstFragment = new FirstFragment();
        Bundle firstBundle = new Bundle();
        firstBundle.putString("trackrecord",gson.toJson(trackRecord));
        firstFragment.setArguments(firstBundle);

        adapter.addFragment(firstFragment, "Quizz");
        adapter.addFragment(new SecondFragment(), "Words list");
        adapter.addFragment(new ThirdFragment(), "Upload");
        adapter.addFragment(new FourthFragment(), "Settings");
        //set apdater for view pager
        viewPager.setAdapter(adapter);


    }

    @Override
    public void onFirstFragmentInteraction(Uri uri){

    }

    @Override
    public void onSecondFragmentInteraction(Uri uri){

    }

    @Override
    public void onThirdFragmentInteraction(Uri uri){

    }

    @Override
    public void onFourthFragmentInteraction(Uri uri){

    }

    private void initFromPref(){
        finToEng = new Dictionary("fin","eng");
        engToFin = new Dictionary("eng","fin");
        quizzLength = 6;
        user = new User("default");

        Object
            finToEngPref = SharedPreferencesManager
                    .getInstance(this)
                    .loadFromPref(Dictionary.class,Constant.DICTIONARY_PREF,Constant.FIN_TO_EN_PREF_KEY),
            engToFinPref=SharedPreferencesManager
                    .getInstance(this)
                    .loadFromPref(Dictionary.class,Constant.DICTIONARY_PREF,Constant.EN_TO_FIN_PREF_KEY),
            quizzLengthPref = SharedPreferencesManager
                    .getInstance(this)
                    .loadFromPref(Integer.class, Constant.DEFAULT, Constant.QUIZZLENGTH_KEY);

        if (null != finToEngPref )
            finToEng = (Dictionary)finToEngPref;
        if (null != engToFinPref)
            engToFin = (Dictionary)engToFinPref;
        if (null != quizzLengthPref)
            quizzLength = (Integer)quizzLengthPref;

/*
        RandomQuiz fullSetQuizFinEn = new RandomQuiz(finToEng.getWordCount(),finToEng);
        RandomQuiz fullSetQuizEnFin = new RandomQuiz(finToEng.getWordCount(),engToFin);
        user.addQuiz(fullSetQuizEnFin);
        user.addQuiz(fullSetQuizFinEn);
   */
        UserData u = MyModelRoot.getInstance().getUserData();
        u.setUser(user);
        u.addDictionary("fineng", finToEng);
        u.addDictionary("engfin", engToFin);
        MyModelRoot.getInstance().setUserData(u);
    }

    /*
    * Custom adapter for view pager
    * */
    private class ViewPagerAdapter extends FragmentPagerAdapter{
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager){
            super(manager);
        }

        @Override
        public Fragment getItem(int pos){
            return mFragmentList.get(pos);
        }

        @Override
        public int getCount(){
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title){
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int pos){
            return mFragmentTitleList.get(pos);
        }


    }



}
