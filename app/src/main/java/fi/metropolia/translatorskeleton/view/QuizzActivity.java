package fi.metropolia.translatorskeleton.view;


import android.content.DialogInterface;
import android.content.Intent;

import android.os.AsyncTask;

import android.os.SystemClock;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.StringBuilderPrinter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;


import org.apache.axis.constants.Use;
import org.json.JSONObject;

import java.util.Map;
import java.util.Set;

import fi.metropolia.translatorskeleton.R;
import fi.metropolia.translatorskeleton.helper.Mapper;
import fi.metropolia.translatorskeleton.model.DictionaryController;
import fi.metropolia.translatorskeleton.model.HardQuiz;
import fi.metropolia.translatorskeleton.model.MyModelRoot;
import fi.metropolia.translatorskeleton.model.Quiz;
import fi.metropolia.translatorskeleton.model.QuizItem;
import fi.metropolia.translatorskeleton.model.RandomQuiz;
import fi.metropolia.translatorskeleton.model.TimeOutObserver;
import fi.metropolia.translatorskeleton.model.TimeOutQuestion;

import fi.metropolia.translatorskeleton.model.TrackRecord;
import fi.metropolia.translatorskeleton.model.User;
import fi.metropolia.translatorskeleton.model.UserData;
import fi.metropolia.translatorskeleton.utility.FlickrManager;
import fi.metropolia.translatorskeleton.utility.SharedPreferencesManager;
import fi.metropolia.translatorskeleton.helper.Constant;
import fi.metropolia.translatorskeleton.model.Dictionary;


public class QuizzActivity extends AppCompatActivity {

    private TextView questionTextView;
    private EditText answerEditText;
    private ImageView imageView;
    private ProgressBar progressBar;
    private TextView currentScoreTextView;
    private TextView highScoreTextView;
    private Quiz quizz;
    Dictionary dict;
    private int currentQuiz = 0;
    private TrackRecord trackRecord;
    private FlickrManager flickrManager;
    private TimeOutQuestion timeOutQuestion;
    private TimeOutObserver timeOutObserver;
    private String quizzType;
    private int quizzLength;
    private int timeOut;
    private int currentScore = 0;
    private int highScore;
    private int pointGain;
    TimeoutTask timeoutTask;
    UpdateProgressTask updateProgressTask;
    User user;
    UserData userData;
    DictionaryController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quizz);

        //Default values for variables that are going to loaded from shared prefs
        controller = new DictionaryController(this.getApplicationContext());
        int type = getIntent().getExtras().getInt("type");
        dict = type == 0 ? new Dictionary("fin", "eng") : new Dictionary("eng", "fin");
        highScore = 0;
        quizzLength = Constant.QUIZZLENGTH_THRESHOLD;
        timeOut = Constant.TIMEOUT_THRESHOLD;
        quizzType = "random";
        prepareFromPref();

        userData = MyModelRoot.getInstance().getUserData();
        user = userData.getUser();
        // user = controller.reloadUser();

        flickrManager = new FlickrManager(this.getApplicationContext());
        trackRecord = user.getTrackRecord();

        //Hack to prevent crash ...
        //RandomQuiz tempQuiz = new RandomQuiz(dict.getWordCount(),dict);
        //trackRecord.add(tempQuiz);


        TextView titleView = (TextView) findViewById(R.id.quizz_view_title);
        questionTextView = (TextView) findViewById(R.id.question_textView);
        answerEditText = (EditText) findViewById(R.id.answer_editText);
        Button submitButton = (Button) findViewById(R.id.submit_answer_button);
        imageView = (ImageView) findViewById(R.id.quizz_imageView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        currentScoreTextView = (TextView) findViewById(R.id.current_score_textview);
        highScoreTextView = (TextView) findViewById(R.id.high_score_textview);

        String title;
        if (quizzType.equals("random")) {
            title = "RANDOM";
            quizz = new RandomQuiz(quizzLength, dict);
        } else {
            title = "HARD";

            quizz = new HardQuiz(quizzLength, dict, trackRecord);
        }

        //The lower the timeout, the higher the points gained
        if (timeOut <= 10)
            pointGain = 15;
        else if (10 < timeOut && timeOut < 20)
            pointGain = 10;
        else
            pointGain = 5;

         /*
        * Setup components
        * */
        titleView.setText(title);
        currentScoreTextView.setText(Integer.toString(currentScore));
        highScoreTextView.setText(Integer.toString(highScore));
        progressBar.setMax(timeOut * 10);
        String initialQuestion = quizz.getItem(currentQuiz).getQuestion();
        questionTextView.setText(initialQuestion);
        flickrManager.placeImage((String) Mapper.getInstance().getMap().get(initialQuestion), imageView);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleQuiz(quizz);
            }
        });

        setupThreadingTasks();


    }

    /*
  * Save current track record to shared pref to reuse later
  * Save high score to shared pref
  * Remove observer from timeoutquestion
  * */
    @Override
    protected void onStop() {
        super.onStop();
        user.addQuiz(quizz);
        //controller.saveUser();
        userData.setUser(user);
        HardQuiz quiz = new HardQuiz(quizzLength,dict,user.getTrackRecord());
        MyModelRoot.getInstance().setUserData(userData);
        MyModelRoot.getInstance().getUserData().setUser(user);
        SharedPreferencesManager
                .getInstance(this).saveToPref(highScore, Constant.DEFAULT, Constant.HIGH_SCORE_KEY);
        timeOutQuestion.removeTimeOutObserver(timeOutObserver);
        timeoutTask.cancel(true);
        updateProgressTask.cancel(true);

    }

    /*
           * Assign quizz length, dictionary, timeout, high score values from shared ref
           * Assign min value if ref is null
    *
    * */
    private void prepareFromPref() {
        int type = getIntent().getExtras().getInt("type");
        String key;
        if (type == 0)
            key = Constant.FIN_TO_EN_PREF_KEY;
        else
            key = Constant.EN_TO_FIN_PREF_KEY;
        Object

                dictPref = SharedPreferencesManager
                .getInstance(this)
                .loadFromPref(Dictionary.class, Constant.DICTIONARY_PREF,
                        key),
                quizzLengthPref = SharedPreferencesManager
                        .getInstance(this)
                        .loadFromPref(Integer.class, Constant.DEFAULT, Constant.QUIZZLENGTH_KEY),
                timeOutPref = SharedPreferencesManager
                        .getInstance(this)
                        .loadFromPref(Integer.class, Constant.DEFAULT, Constant.TIMEOUT_KEY),
                highScorePref = SharedPreferencesManager
                        .getInstance(this)
                        .loadFromPref(Integer.class, Constant.DEFAULT, Constant.HIGH_SCORE_KEY),
                quizzTypePref = SharedPreferencesManager
                        .getInstance(this)
                        .loadFromPref(String.class, Constant.RADIO_GROUP_KEY, Constant.DEFAULT);

        if (null != dictPref) {
            dict = (Dictionary) dictPref;
        }
        if (null != quizzLengthPref) {
            quizzLength = (Integer) quizzLengthPref;
        }
        if (null != timeOutPref) {
            timeOut = (Integer) timeOutPref;
        }
        if (null != highScorePref) {
            highScore = (Integer) highScorePref;
        }
        if (null != quizzTypePref) {
            quizzType = (String) quizzTypePref;
        }


    }

    private void setupThreadingTasks() {
        timeOutObserver = new TimeOutObserver() {
            @Override
            public void timeout() {
                //Log.d("timeout done", "tick tick tick");

            }
        };
        timeOutQuestion = new TimeOutQuestion(timeOut * 1000);
        timeOutQuestion.registerTimeOutObserver(timeOutObserver);
        timeoutTask = new TimeoutTask();
        updateProgressTask = new UpdateProgressTask();
        /*
        * Run 2 tasks on a thread pool
        * So that they can execute in parallel
        * */
        timeoutTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, timeOutQuestion);
        updateProgressTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, timeOut);

    }

    /*
    * Kill running tasks
    * Create new task and start again
    * */
    private void restartTasks() {
        timeoutTask.cancel(true);
        timeoutTask = new TimeoutTask();
        timeoutTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, timeOutQuestion);

        updateProgressTask.cancel(true);
        updateProgressTask = new UpdateProgressTask();
        updateProgressTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, timeOut);
    }


    /*
    * Reset scene and components
    * */

    private void resetScene() {

        currentQuiz = 0;
        currentScore = 0;
        quizz = new RandomQuiz(quizzLength, dict);
        String initialQuestion = quizz.getItem(currentQuiz).getQuestion();
        questionTextView.setText(initialQuestion);
        answerEditText.setText("");
        currentScoreTextView.setText(Integer.toString(currentScore));
        flickrManager.placeImage((String) Mapper.getInstance().getMap().get(initialQuestion), imageView);
        restartTasks();
    }

    //TODO: refactor later
    private void handleQuiz(Quiz quiz) {
        String ans = answerEditText.getText().toString();
        if (quiz.checkAnswer(currentQuiz, ans)) {
            if (currentQuiz >= quizzLength - 1) {
                currentQuiz = 0;
                String message = "Score: " + Integer.toString(currentScore) + "\nRestart?";
                AlertDialog alert = alertDialogBuilder("GAME OVER", message).create();
                alert.show();
            } else
                ++currentQuiz;
            handleSuccessfulQuiz(quiz);
        } else
            handleFailQuiz();


    }

    private void handleSuccessfulQuiz(Quiz quiz) {
        currentScore += pointGain;
        QuizItem item = quiz.getItem(currentQuiz);
        questionTextView.setText(item.getQuestion());
        answerEditText.setText("");
        currentScoreTextView.setText(Integer.toString(currentScore));
        if (currentScore > highScore) {
            highScore = currentScore;
            highScoreTextView.setText(Integer.toString(highScore));
        }
        Log.d("answer:", "yay dung cmnr");
        String id = Mapper.getInstance().getMap().get(item.getQuestion());
        flickrManager.placeImage(id, imageView);

        restartTasks();
    }

    private void handleFailQuiz() {
        Log.d("answer:", "ngu vkl");
        AlertDialog alert = alertDialogBuilder("GAME OVER", "Restart ?").create();
        alert.show();
        updateProgressTask.cancel(true);
        timeoutTask.cancel(true);
    }


    /**
     * Asynctask class
     * Handle updating progress bar
     */
    private class UpdateProgressTask extends AsyncTask<Integer, Integer, String> {
        private volatile boolean running;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            running = true;
        }

        @Override
        protected String doInBackground(Integer... params) {
            int cur = params[0];
            for (int i = 0; i <= cur * 2; i++) {
                try {
                    {
                        publishProgress(i * 5);
                        Thread.sleep(500);
                    }
                } catch (InterruptedException e) {
                    //e.printStackTrace();
                }
                if (isCancelled())
                    break;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressBar.setProgress(0);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progressBar.setProgress(values[0]);

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            running = false;
            progressBar.setProgress(0);
        }


    }

    /*
    * Async task
    * Handle running timeout question
    * Runs in parallel with update progress bar task
    * */
    private class TimeoutTask extends AsyncTask<TimeOutQuestion, Void, Void> {
        private volatile boolean running;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            running = true;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            AlertDialog alert = alertDialogBuilder("GAME OVER", "Restart ?").create();
            alert.show();
        }

        @Override
        protected Void doInBackground(TimeOutQuestion... params) {
            TimeOutQuestion quest = params[0];
            if (isCancelled())
                return null;
            quest.run();
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            running = false;
        }
    }

    private AlertDialog.Builder alertDialogBuilder(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                resetScene();
                dialog.cancel();
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        return builder;
    }
}



