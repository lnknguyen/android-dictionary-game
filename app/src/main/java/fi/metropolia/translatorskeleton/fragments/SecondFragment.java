package fi.metropolia.translatorskeleton.fragments;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import fi.metropolia.translatorskeleton.R;
import fi.metropolia.translatorskeleton.helper.Constant;
import fi.metropolia.translatorskeleton.helper.VolleyService;
import fi.metropolia.translatorskeleton.model.Dictionary;
import fi.metropolia.translatorskeleton.model.MyModelRoot;
import fi.metropolia.translatorskeleton.model.RandomQuiz;
import fi.metropolia.translatorskeleton.model.TrackRecord;
import fi.metropolia.translatorskeleton.utility.SharedPreferencesManager;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SecondFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SecondFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SecondFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String keys[] = {};
    private ArrayList<String> negativeArray = new ArrayList<>();
    private String finToEnUrl ="https://translate.google.com/#fi/en/";
    private String enToFinUrl ="https://translate.google.com/#en/fi/";
    private Dictionary finToEnDict,enToFinDict;
    private ListViewAdapter adapter;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private OnFragmentInteractionListener mListener;
    private TrackRecord trackRecord;
    public SecondFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SecondFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SecondFragment newInstance(String param1, String param2) {
        SecondFragment fragment = new SecondFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    //TODO: Dynamic binding to array
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_second, container, false);
        ListView listView = (ListView)view.findViewById(R.id.dictionary_listView);
        View footerView = inflater.inflate(R.layout.footer_view,listView,false);
        listView.addHeaderView(footerView);
         finToEnDict = SharedPreferencesManager
                .getInstance(this.getContext())
                .loadFromPref(Dictionary.class,Constant.DICTIONARY_PREF,Constant.FIN_TO_EN_PREF_KEY);
        enToFinDict = SharedPreferencesManager
                .getInstance(this.getContext())
                .loadFromPref(Dictionary.class,Constant.DICTIONARY_PREF,Constant.EN_TO_FIN_PREF_KEY);
       /*
        trackRecord = MyModelRoot.getInstance().getUserData().getUser().getTrackRecord();
        RandomQuiz tempQuiz = new RandomQuiz(dict.getWordCount(),dict);
        trackRecord.add(tempQuiz);
         negativeArray = trackRecord.getNegatives(dict);
        */
        final Button finToEnButton = (Button)footerView.findViewById(R.id.fin_to_en_button);
        final Button enToFinButton = (Button)footerView.findViewById(R.id.en_to_fin_button);

        adapter = new ListViewAdapter(keys,negativeArray);

        listView.setAdapter(adapter);

        finToEnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (finToEnDict != null) {
                    keys = finToEnDict
                            .getKeys()
                            .toArray(new String[enToFinDict.getWordCount()]);
                }
                adapter.changeData(keys,negativeArray);
            }
        });

        enToFinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (finToEnDict != null) {
                    keys = enToFinDict
                            .getKeys()
                            .toArray(new String[finToEnDict.getWordCount()]);
                }
                adapter.changeData(keys,negativeArray);
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (keys!=null) {
                    String wordSearch = keys[position];
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(finToEnUrl + wordSearch));
                    startActivity(intent);
                }
                return false;
            }
        });

        return view;
    }

    @Override
    public void onResume(){
        super.onResume();
        //Reload every time view is resumed
        if (finToEnDict != null) {
            keys = finToEnDict
                    .getKeys()
                    .toArray(new String[enToFinDict.getWordCount()]);
        }
//        trackRecord = MyModelRoot.getInstance().getUserData().getUser().getTrackRecord();
//         if (trackRecord.getNegatives(dict) != null)
//         negativeArray = trackRecord.getNegatives(dict);
        adapter.changeData(keys,negativeArray);


    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onSecondFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onSecondFragmentInteraction(Uri uri);
    }

    private class ListViewAdapter extends BaseAdapter{

        private String[] mData;
        private ArrayList<String> mNegatives;
        private LayoutInflater mInflater;
        public ListViewAdapter(String[] data, ArrayList<String> negatives){
            mData = data;
            mNegatives = negatives;
            mInflater = LayoutInflater.from(getActivity());
        }

        public void changeData(String[] data, ArrayList<String> negatives){
            mData = data;
            mNegatives = negatives;
            notifyDataSetChanged();
        }
        @Override
        public int getCount() {
            return mData.length;
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView==null){
                convertView = mInflater.inflate(R.layout.dictionary_list_item,parent,false);
            }
            TextView wordTextView = (TextView)convertView.findViewById(R.id.textView9);
            ImageView imageView = (ImageView)convertView.findViewById(R.id.checked_imageView);
            wordTextView.setText(mData[position]);
            imageView.setVisibility(View.INVISIBLE);
            /*
            if (trackRecord.getNegatives(finToEnDict).contains(mData[position]))
                imageView.setVisibility(View.INVISIBLE);
            else
                imageView.setVisibility(View.VISIBLE);
                */
            return convertView;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Object getItem(int position) {
            return mData[position];
        }
    }
}

