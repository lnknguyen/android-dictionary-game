package fi.metropolia.translatorskeleton.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.gson.Gson;

import fi.metropolia.translatorskeleton.R;
import fi.metropolia.translatorskeleton.helper.Constant;
import fi.metropolia.translatorskeleton.model.DictionaryController;
import fi.metropolia.translatorskeleton.model.MyModelRoot;
import fi.metropolia.translatorskeleton.model.TrackRecord;
import fi.metropolia.translatorskeleton.utility.SharedPreferencesManager;
import fi.metropolia.translatorskeleton.view.QuizzActivity;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FirstFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FirstFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FirstFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static DictionaryController dictController;
    private TrackRecord trackRecord;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String quizzType;
    private OnFragmentInteractionListener mListener;

    public FirstFragment(){

    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FirstFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FirstFragment newInstance(String param1, String param2) {
        FirstFragment fragment = new FirstFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        Bundle args = getArguments();
        trackRecord = MyModelRoot.getInstance().getUserData().getUser().getTrackRecord();


        Object quizzTypePref = SharedPreferencesManager
                .getInstance(this.getContext())
                .loadFromPref(String.class, Constant.RADIO_GROUP_KEY,Constant.DEFAULT);
        if (null != quizzTypePref)
            quizzType = (String)quizzTypePref;
        else
            quizzType = "random";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_first, container, false);
        Button finToEnButton = (Button)view.findViewById(R.id.random_quizz_button);
        Button enToFinButton = (Button)view.findViewById(R.id.hard_quizz_button);
        final  Intent intent = new Intent(getActivity(), QuizzActivity.class);
        intent.putExtra("trackrecord",trackRecord);
        finToEnButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (validate() == false)
                {
                    AlertDialog alert = alertDialogBuilder("Sorry","No quizz to play. \nYou have to play random quizz first").create();
                    alert.show();
                    return;
                }
                intent.putExtra("type",0);
                startActivity(intent);
            }
        });

        enToFinButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (validate() == false)
                {
                    AlertDialog alert = alertDialogBuilder("Sorry","No quizz to play. \n" +
                            "You have to play random quizz first").create();
                    alert.show();
                    return;
                }
                intent.putExtra("type", 1);
                startActivity(intent);
            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFirstFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume(){
        super.onResume();
        Object quizzTypePref = SharedPreferencesManager
                .getInstance(this.getContext())
                .loadFromPref(String.class, Constant.RADIO_GROUP_KEY,Constant.DEFAULT);
        if (null != quizzTypePref)
            quizzType = (String)quizzTypePref;
        else
            quizzType = "random";
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFirstFragmentInteraction(Uri uri);
    }

    private AlertDialog.Builder alertDialogBuilder(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        return builder;
    }
    //Make sure that track record has value if quizz type is hard, otherwise will cause crash
    private boolean validate(){
        if (quizzType.equals("hard")){
            if (trackRecord==null || trackRecord.getWordCount()<=0)
                return false;
        }
        return true;
    }
}
