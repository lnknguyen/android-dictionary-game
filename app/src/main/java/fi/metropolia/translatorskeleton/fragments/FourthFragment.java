package fi.metropolia.translatorskeleton.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import fi.metropolia.translatorskeleton.R;
import fi.metropolia.translatorskeleton.helper.Constant;
import fi.metropolia.translatorskeleton.utility.SharedPreferencesManager;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FourthFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FourthFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FourthFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FourthFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FourthFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FourthFragment newInstance(String param1, String param2) {
        FourthFragment fragment = new FourthFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    //TODO: refactor these codes
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fourth, container, false);


        Integer quizzLength=Constant.QUIZZLENGTH_THRESHOLD, timeOut=Constant.TIMEOUT_THRESHOLD;
        String buttonChoice = "random";
        Object quizzLenghtPref = SharedPreferencesManager
                .getInstance(this.getContext()).loadFromPref(Integer.class,Constant.DEFAULT,Constant.QUIZZLENGTH_KEY),
                timeoutPref = SharedPreferencesManager
                        .getInstance(this.getContext()).loadFromPref(Integer.class,Constant.DEFAULT,Constant.TIMEOUT_KEY),
                radioButtonPref = SharedPreferencesManager
                        .getInstance(this.getContext()).loadFromPref(String.class, Constant.RADIO_GROUP_KEY,Constant.DEFAULT);

        /*
        * Assign values from shared prefs to local vars
        * Variable will hold default value if pref is null
        * */
        if (null != quizzLenghtPref)
            quizzLength = (Integer)quizzLenghtPref;
        if (null != timeoutPref)
            timeOut = (Integer)timeoutPref;
        if (null != radioButtonPref)
            buttonChoice = (String)radioButtonPref;

        //Setup quizzlength seekbar and display value
        SeekBar quizzLenghtSeekBar = (SeekBar)view.findViewById(R.id.quizz_length_seek_bar);
        final TextView quizzLengthValue = (TextView)view.findViewById(R.id.quizz_length_value);
        //quizzLenghtSeekBar.setProgress(sharedPref.getInt(Constant.QUIZZLENGTH_KEY,6));
        quizzLenghtSeekBar.setProgress(quizzLength-Constant.QUIZZLENGTH_THRESHOLD);
        quizzLengthValue.setText(Integer.toString(quizzLength));
        quizzLenghtSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                // TODO Auto-generated method stub
                Integer currentProgress = progress + Constant.QUIZZLENGTH_THRESHOLD;
                quizzLengthValue.setText(Integer.toString(currentProgress));
                SharedPreferencesManager
                        .getInstance(getContext())
                        .saveToPref(currentProgress,Constant.DEFAULT,Constant.QUIZZLENGTH_KEY);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }


        });

        //Setup timeoutlength seekbar and display value
        //use a shared preference to save and load changes
        SeekBar timeOutSeekBar = (SeekBar)view.findViewById(R.id.time_out_seek_bar);
        final TextView timeOutValue = (TextView)view.findViewById(R.id.time_out_value);
        timeOutSeekBar.setProgress(timeOut-Constant.TIMEOUT_THRESHOLD);//optional value is 6 if no value is found in shared pref
        timeOutValue.setText(Integer.toString(timeOut));
        timeOutSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                // TODO Auto-generated method stub\
                Integer currentProgress = progress + Constant.TIMEOUT_THRESHOLD;
                timeOutValue.setText(Integer.toString(currentProgress));
                SharedPreferencesManager
                        .getInstance(getContext())
                        .saveToPref(currentProgress,Constant.DEFAULT,Constant.TIMEOUT_KEY);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }


        });

        //setup radio button group
        //random by default
        RadioGroup groupButton = (RadioGroup)view.findViewById(R.id.radio_group);
        if (buttonChoice.equals("hard"))
        //if (sharedPref.getString(Constant.RADIO_GROUP_KEY,"default").equals("hard"))
            groupButton.check(R.id.hard_button);

        else
            groupButton.check(R.id.random_button);


        groupButton.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case (R.id.random_button):
                        SharedPreferencesManager.getInstance(getContext()).saveToPref("random",Constant.RADIO_GROUP_KEY,Constant.DEFAULT);
                        break;

                    case (R.id.hard_button):
                        SharedPreferencesManager.getInstance(getContext()).saveToPref("hard",Constant.RADIO_GROUP_KEY,Constant.DEFAULT);
                        break;
                }
            }
        });
        // Inflate the layout for this fragment

        return view;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFourthFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFourthFragmentInteraction(Uri uri);
    }


}
